import { Model } from "mongoose";
import { ObjectId } from "bson";

import { ToDo } from "./../todos/interfaces/Todos.types";
import { Status } from "../todos/interfaces/Status.enum";
import { ToDoModel } from "./../todos/todos.model";
import { StatusError } from "../core/Errors";
import { PomoSessionRaw, PomoSession } from "./interfaces/pomoSession.types";

export class pomoSessionService {
  constructor(private readonly pomoSessionModel: Model<PomoSessionRaw>) {}

  async create(userId: string): Promise<PomoSessionRaw> {
    const currentTasks: ToDo[] = await ToDoModel.find({ userId }).lean();
    const finished = <ToDo[]>[];

    const pomoSession = {
      userId,
      time: "25:00",
      todos: currentTasks,
      finished,
      status: Status.ONGOING
    };

    return await this.pomoSessionModel.create(pomoSession);
  }

  async getOne(pomoSessionId: string): Promise<PomoSessionRaw | null> {
    return await this.pomoSessionModel.findById(new ObjectId(pomoSessionId));
  }

  async edit(
    pomoSessionId: string,
    params: PomoSession,
    userId: string
  ): Promise<PomoSessionRaw> {
    const userTasks: ToDo[] = await ToDoModel.find({ userId }).lean();

    const activeTasks: ToDo[] = userTasks.filter(
      el => el.status === Status.ONGOING
    );
    const finishedTasks: ToDo[] = userTasks.filter(
      el => el.status === Status.DONE
    );

    const pomoSession = await this.pomoSessionModel.findOneAndUpdate(
      { _id: new ObjectId(pomoSessionId) },
      { $set: { todos: activeTasks, finished: finishedTasks, ...params } }
    );

    if (!pomoSession) {
      throw new StatusError(404, "Pomo Session not found");
    }

    return pomoSession;
  }

  async delete(pomoSessionId: string): Promise<PomoSessionRaw | null> {
    return await this.pomoSessionModel.findOneAndDelete(
      new ObjectId(pomoSessionId)
    );
  }

  async getActivePomoSession(userId: string): Promise<PomoSessionRaw> {
    const pomoSession = await this.pomoSessionModel.findOne({
      userId,
      status: Status.ONGOING
    });

    if (!pomoSession) {
      throw new StatusError(404, "No active Pomo Sessions");
    }

    return pomoSession;
  }
}

export const createPomoSessionService = (
  pomoSessionModel: Model<PomoSessionRaw>
) => {
  return new pomoSessionService(pomoSessionModel);
};
