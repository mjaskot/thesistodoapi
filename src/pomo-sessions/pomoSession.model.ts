import { prop, Typegoose } from "typegoose";

import { ToDo } from "todos/interfaces/Todos.types";
import { Status } from "../todos/interfaces/Status.enum";

class PomoSession extends Typegoose {
  @prop({ required: true })
  userId: string;

  @prop({ required: true })
  time: string;

  @prop({ required: true })
  todos: [ToDo];

  @prop()
  finished: [ToDo];

  @prop()
  status: Status;
}

export const PomoSessionModel = new PomoSession().getModelForClass(
  PomoSession,
  {
    schemaOptions: { collection: "PomoSession", timestamps: true }
  }
);
