import { celebrate, Joi } from "celebrate";
import { Status } from "../../todos/interfaces/Status.enum";

export const createUpdatePomoSessionValidator = () =>
  celebrate({
    body: Joi.object()
      .keys({
        time: Joi.string(),
        status: Joi.string()
          .valid([Status.DONE, Status.ONGOING])
          .min(4)
          .max(7)
      })
      .required()
  });
