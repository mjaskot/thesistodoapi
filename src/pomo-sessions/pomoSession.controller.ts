import { Response, NextFunction, Request } from "express";

import { pomoSessionService } from "./pomosession.service";

export class PomoSessionController {
  constructor(private readonly pomoSessionService: pomoSessionService) {}

  createPomoSession = async (
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    try {
      return res
        .status(200)
        .json(await this.pomoSessionService.create(req.user._id));
    } catch (err) {
      return next(err);
    }
  };
  getActivePomoSession = async (
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    try {
      return res
        .status(200)
        .json(await this.pomoSessionService.getActivePomoSession(req.user._id));
    } catch (err) {
      return next(err);
    }
  };
  getPomoSession = async (req: Request, res: Response, next: NextFunction) => {
    try {
      return res
        .status(200)
        .json(await this.pomoSessionService.getOne(req.params.pomoSessionId));
    } catch (err) {
      return next(err);
    }
  };

  editPomoSession = async (req: Request, res: Response, next: NextFunction) => {
    try {
      return res
        .status(200)
        .json(
          await this.pomoSessionService.edit(
            req.params.pomoSessionId,
            req.body,
            req.user._id
          )
        );
    } catch (err) {
      return next(err);
    }
  };

  deletePomoSession = async (
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    try {
      return res
        .status(200)
        .json(await this.pomoSessionService.delete(req.params.pomoSessionId));
    } catch (err) {
      return next(err);
    }
  };
}

export const createPomoSessionsController = (
  pomoSessionService: pomoSessionService
) => {
  return new PomoSessionController(pomoSessionService);
};
