import { Document } from "mongoose";

import { ToDo } from "todos/interfaces/Todos.types";
import { Status } from "../../todos/interfaces/Status.enum";

export type PomoSession = {
  userId: string;
  time: string;
  todos: [ToDo];
  finished: [ToDo];
  status: Status;
};

export type PomoSessionRaw = PomoSession & Document;
