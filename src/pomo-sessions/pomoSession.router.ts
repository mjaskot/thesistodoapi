import { compose } from "ramda";
import { Router } from "express";

import { PomoSessionModel } from "./pomoSession.model";
import { createPomoSessionsController } from "./pomosession.controller";
import { createPomoSessionService } from "./pomosession.service";
import { authorize } from "../auth/middlewares/authorize.middleware";

export const createPomoSessionRouter = (): Router => {
  const pomoSessionRouter = Router();

  const pomoSessionController = compose(
    createPomoSessionsController,
    createPomoSessionService
  )(PomoSessionModel);

  pomoSessionRouter
    .route("/")
    .get(authorize, pomoSessionController.getActivePomoSession)
    .post(authorize, pomoSessionController.createPomoSession);

  pomoSessionRouter
    .route("/:pomoSessionId")
    .get(authorize, pomoSessionController.getPomoSession)
    .put(authorize, pomoSessionController.editPomoSession)
    .delete(pomoSessionController.deletePomoSession);

  return pomoSessionRouter;
};
