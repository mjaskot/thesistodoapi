import express = require("express");
import bodyParser = require("body-parser");
import { errors } from "celebrate";
import { Request, Response, NextFunction } from "express";

import { StatusError } from "./core/Errors";
import { createToDoRouter } from "./todos/todos.router";
import { createPomoSessionRouter } from "./pomo-sessions/pomoSession.router";
import { createAuthRouter, createUserRouter } from "./auth/auth.router";

const app = express();

app.use(bodyParser.json());
app.use("/auth", createAuthRouter());
app.use("/users", createUserRouter());
app.use("/todos", createToDoRouter());
app.use("/pomoSessions", createPomoSessionRouter());
app.use(errors());
app.use(
  (err: StatusError, _req: Request, res: Response, _next: NextFunction) => {
    res.status(err.status).json({ Message: err.message });
  }
);

export default app;
