export class StatusError extends Error {
  status: number;
  constructor(code: number, message: string, stack?: string) {
    super(message);
    this.status = code;
    this.stack = stack;
  }
}
