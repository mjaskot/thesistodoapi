require("dotenv").config();

export enum envVariables {
  DBURL = "DB_URL",
  APIDOMAIN = "API_DOMAIN",
  DBNAME = "DB_NAME",
  PORT = "PORT",
  SECRET = "SECRET",
  HASHSECRET = "HASH_SECRET",
  HASHINGALGORITHM = "HASHING_ALGORITHM",
  MAILGUNAPIKEY = "MAILGUN_API_KEY",
  MAILGUNDOMAIN = "MAILGUN_DOMAIN",
  CRYPTOIV = "CRYPTO_IV"
}

export const getEnvVariable = (variable: envVariables) => {
  const envVariable = process.env[variable];

  if (!envVariable) {
    throw new Error(`Error loading environmental variable ${variable}`);
  }

  return envVariable;
};
