import { compose } from "ramda";
import { Router } from "express";

import { ToDoModel } from "./todos.model";
import { authorize } from "./../auth/middlewares/authorize.middleware";
import { createToDoService } from "./todos.service";
import { createToDoController } from "./todos.controller";
import { createAddToDoValidator } from "./validators/createToDo.validator";
import { createUpdateToDoValidator } from "./validators/updateToDoValidator";

export const createToDoRouter = (): Router => {
  const toDoRouter = Router();

  const toDoController = compose(
    createToDoController,
    createToDoService
  )(ToDoModel);

  toDoRouter
    .route("/")
    .post(authorize, createAddToDoValidator(), toDoController.createToDo);

  toDoRouter
    .route("/:toDoId")
    .get(authorize, toDoController.getToDo)
    .put(authorize, createUpdateToDoValidator(), toDoController.updateToDo)
    .delete(authorize, toDoController.removeToDo);

  toDoRouter.route("/").get(authorize, toDoController.getAllToDosPerUser);

  return toDoRouter;
};
