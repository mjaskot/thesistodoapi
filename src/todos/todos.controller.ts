import { Response, NextFunction, Request } from "express";

import { ToDoService } from "./todos.service";

class ToDoController {
  constructor(private readonly toDoService: ToDoService) {}

  createToDo = async (req: Request, res: Response, next: NextFunction) => {
    try {
      return res
        .status(200)
        .json(await this.toDoService.createOne(req.body, req.user._id));
    } catch (err) {
      return next(err);
    }
  };

  getAllToDosPerUser = async (
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    try {
      return res
        .status(200)
        .json(await this.toDoService.getAllPerUser(req.user._id));
    } catch (err) {
      return next(err);
    }
  };

  getToDo = async (req: Request, res: Response, next: NextFunction) => {
    try {
      return res
        .status(200)
        .json(await this.toDoService.getOne(req.query.toDoId));
    } catch (err) {
      return next(err);
    }
  };

  updateToDo = async (req: Request, res: Response, next: NextFunction) => {
    try {
      return res
        .status(200)
        .json(await this.toDoService.editOne(req.params.toDoId, req.body));
    } catch (err) {
      return next(err);
    }
  };

  removeToDo = async (req: Request, res: Response, next: NextFunction) => {
    try {
      return res
        .status(200)
        .json(await this.toDoService.deleteOne(req.params.toDoId));
    } catch (err) {
      return next(err);
    }
  };
}

export const createToDoController = (
  toDoService: ToDoService
): ToDoController => new ToDoController(toDoService);
