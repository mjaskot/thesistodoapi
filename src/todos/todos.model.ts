import { prop, Typegoose } from "typegoose";

import { Status } from "../todos/interfaces/Status.enum";

class ToDoSchema extends Typegoose {
  @prop({ required: true })
  description: string;

  @prop()
  userId: string;

  @prop({ enum: Status })
  status: Status;
}

export const ToDoModel = new ToDoSchema().getModelForClass(ToDoSchema, {
  schemaOptions: { timestamps: true, collection: "ToDos" }
});
