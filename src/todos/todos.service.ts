import { Model } from "mongoose";
import { ObjectId } from "bson";

import { ToDoRaw, ToDo } from "./interfaces/Todos.types";
import { StatusError } from "./../core/Errors";

export class ToDoService {
  constructor(private readonly ToDoModel: Model<ToDoRaw>) {}

  async createOne(params: ToDo, userId: string) {
    const toDo: ToDo = {
      description: params.description,
      userId,
      status: params.status
    };
    return await this.ToDoModel.create(toDo);
  }

  async getAllPerUser(userId: string) {
    return await this.ToDoModel.find({ userId });
  }

  async getOne(toDoId: string) {
    return await this.ToDoModel.findById(new ObjectId(toDoId));
  }

  async editOne(toDoId: string, params: ToDoRaw): Promise<ToDoRaw> {
    const toDo = await this.ToDoModel.findOneAndUpdate(
      {
        _id: toDoId
      },
      { $set: { ...params } }
    );

    if (!toDo) {
      throw new StatusError(404, "ToDo not found!");
    }

    return toDo;
  }

  async deleteOne(toDoId: string): Promise<ToDoRaw> {
    const toDo = await this.ToDoModel.findOneAndDelete({ _id: toDoId });

    if (!toDo) {
      throw new StatusError(404, "ToDo not found!");
    }

    return toDo;
  }
}

export const createToDoService = (ToDoModel: Model<ToDoRaw>): ToDoService =>
  new ToDoService(ToDoModel);
