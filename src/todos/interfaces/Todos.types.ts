import { Document } from "mongoose";
import { Status } from "./Status.enum";

export type ToDo = {
  description: string;
  userId: string;
  status: Status;
};

export type ToDoRaw = Document & ToDo;

export type ToDoResponse = {
  description: string;
  status: string;
};
