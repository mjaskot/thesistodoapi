import { celebrate, Joi } from "celebrate";
import { Status } from "../interfaces/Status.enum";

export const createUpdateToDoValidator = () =>
  celebrate({
    body: Joi.object()
      .keys({
        description: Joi.string()
          .min(3)
          .max(50),
        status: Joi.string()
          .valid([Status.DONE, Status.ONGOING])
          .min(4)
          .max(7)
      })
      .required()
  });
