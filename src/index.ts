import app from "./app";

import { createDBConnection } from "./core/db";
import { getEnvVariable, envVariables } from "./core/env.initializer";

const mongoUrl = getEnvVariable(envVariables.DBURL);
const dbName = getEnvVariable(envVariables.DBNAME);
const port = getEnvVariable(envVariables.PORT);

const bootstrap = async () => {
  try {
    createDBConnection(mongoUrl, dbName);
    app.listen(port, () => console.log(`Server listening on port ${port}`));
  } catch (err) {
    console.log(err);
  }
};

bootstrap();
