import { Request, Response, NextFunction } from "express";

import { AuthService } from "./auth.service";

class AuthController {
  constructor(private readonly authService: AuthService) {}

  signIn = async (req: Request, res: Response, next: NextFunction) => {
    const { email, password } = req.body;
    try {
      return res.status(200).json({
        data: await this.authService.login(email, password)
      });
    } catch (err) {
      return next(err);
    }
  };

  register = async (req: Request, res: Response, next: NextFunction) => {
    try {
      return res.status(200).json({
        data: await this.authService.register(req.body)
      });
    } catch (err) {
      return next(err);
    }
  };

  editUser = async (req: Request, res: Response, next: NextFunction) => {
    try {
      return res.status(200).json({
        data: await this.authService.updateUser(req.params.userId, req.body)
      });
    } catch (err) {
      return next(err);
    }
  };

  requestPasswordReset = async (
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    const { email } = req.body;
    try {
      return res.status(200).json({
        data: await this.authService.sendResetPasswordEmail(email)
      });
    } catch (err) {
      return next(err);
    }
  };

  resetPassword = async (req: Request, res: Response, next: NextFunction) => {
    const { password } = req.body;
    try {
      return res.status(200).json({
        data: await this.authService.resetPassword(req.query.token, password)
      });
    } catch (err) {
      return next(err);
    }
  };

  refreshToken = async (req: Request, res: Response, next: NextFunction) => {
    try {
      return res.status(200).json({
        data: {
          accessToken: await this.authService.refreshAccessToken(
            req.body.refreshToken
          )
        }
      });
    } catch (err) {
      return next(err);
    }
  };
}

export const createAuthController = (
  authService: AuthService
): AuthController => new AuthController(authService);
