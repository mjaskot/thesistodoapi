import { Request, Response, NextFunction } from "express";

import { UserModel } from "../models/user.model";
import { getEnvVariable, envVariables } from "../../core/env.initializer";
import { verifyToken } from "../_helpers/token.helpers";
import { StatusError } from "../../core/Errors";
import { UserRaw } from "../interfaces/User.types";

declare global {
  module Express {
    interface Request {
      user: UserRaw;
    }
  }
}

const secret = getEnvVariable(envVariables.SECRET);

export const authorize = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (!req.headers.authorization) {
    return res.status(401).json("Unauthorized");
  }

  if (req.headers.authorization) {
    try {
      const token = req.headers.authorization.split(" ")[1];
      const decoded = await verifyToken(token, secret);

      const user = await UserModel.findOne({
        _id: decoded.uid
      });

      if (!user) {
        throw new StatusError(404, "User not found");
      }

      req.user = user;

      return next();
    } catch (err) {
      return next(err);
    }
  }
};
