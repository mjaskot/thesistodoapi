import { celebrate, Joi } from "celebrate";

export const createRefreshTokenValidator = () =>
  celebrate({
    body: Joi.object()
      .keys({
        refreshToken: Joi.string().required()
      })
      .required()
  });
