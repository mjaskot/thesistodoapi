import { celebrate, Joi } from "celebrate";

export const createPutUserValidator = () =>
  celebrate({
    body: Joi.object()
      .keys({
        username: Joi.string()
          .min(3)
          .max(25),
        email: Joi.string()
          .email()
          .min(2)
          .max(30),
        password: Joi.string()
          .min(5)
          .max(18),
        age: Joi.number()
          .positive()
          .min(16),
        address: Joi.string()
          .min(3)
          .max(30),
        city: Joi.string()
          .min(2)
          .max(30),
        country: Joi.string()
          .min(4)
          .max(30)
      })
      .required()
  });
