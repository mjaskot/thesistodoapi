import { Model } from "mongoose";
import { ObjectID } from "bson";
//import * as Mailgun from "mailgun-js";

import { encryptor } from "./_helpers/crypto/encryptor";
import { decryptor } from "./_helpers/crypto/decryptor";
import { StatusError } from "../core/Errors";
import { createMailgun } from "./_helpers/mailgun";
import { refreshToken } from "./_helpers/token.helpers";
import { toRegisteredUser } from "./_helpers/utilities";
import { LoginResponse, mailGunData } from "./interfaces/Utility.types";
import { hashPassword, checkPassword } from "./_helpers/bcrypt.helpers";
import { getEnvVariable, envVariables } from "./../core/env.initializer";
import { UserRaw, RegisteredUser, User } from "./interfaces/User.types";
import {
  generateAccessToken,
  generateRefreshToken
} from "./_helpers/token.helpers";

const domain = getEnvVariable(envVariables.MAILGUNDOMAIN);
const apiKey = getEnvVariable(envVariables.MAILGUNAPIKEY);
const apiDomain = getEnvVariable(envVariables.APIDOMAIN);
//const secret = getEnvVariable(envVariables.SECRET);

export class AuthService {
  constructor(private readonly userModel: Model<UserRaw>) {}

  async register(params: User): Promise<RegisteredUser> {
    const user = await this.userModel.create({
      ...params,
      password: await hashPassword(params.password, 10),
      role: "user"
    });

    const token = await generateAccessToken(user._id);

    const registeredUser: RegisteredUser = toRegisteredUser(user, token);

    return registeredUser;
  }

  async login(email: string, password: string): Promise<LoginResponse> {
    const user = await this.userModel.findOne({ email });

    const isValid = await checkPassword(password, user ? user.password : null);

    if (!user || !isValid) {
      throw new StatusError(401, "Your login or password is incorrect.");
    }

    const refreshToken = await generateRefreshToken(user._id);

    return {
      accessToken: await generateAccessToken(user._id),
      refreshToken: refreshToken.token
    };
  }

  async refreshAccessToken(refreshtoken: string): Promise<string> {
    return await refreshToken(refreshtoken);
  }

  async updateUser(userId: string, params: User): Promise<User> {
    const user = await this.userModel.findOneAndUpdate(
      { _id: new ObjectID(userId) },
      { $set: { ...params, password: await hashPassword(params.password, 10) } }
    );
    if (!user) {
      throw new StatusError(404, "User not found");
    }
    return user;
  }

  async resetPassword(token: string, pass: string): Promise<LoginResponse> {
    const { userId } = await decryptor(token);

    const user = await this.userModel.findOneAndUpdate(
      { _id: userId },
      { $set: { password: await hashPassword(pass, 10) } }
    );

    if (!user) {
      throw new StatusError(404, "User not found");
    }

    const refreshToken = await generateRefreshToken(user._id);

    return {
      accessToken: await generateAccessToken(user._id),
      refreshToken: refreshToken.token
    };
  }

  sendResetPasswordEmail = async (email: string) => {
    const user = await this.userModel.findOne({ email });

    if (!user) {
      throw new StatusError(404, "User with provided email not found");
    }

    const userId = user._id;
    const token = await encryptor(userId);
    const href = `${apiDomain}/auth/resetPassword?token=${token}`;

    const data: mailGunData = {
      from: "Timeley Administration <jaskotmarcin1@gmail.com>",
      to: email,
      subject: "Password reset request.",
      text: `<h1>Hello</h1><br>
             <p>We got reset password request from this email, to complete refrest password please click link below</p><br>
             <a href="${href}">Click!</a>`
    };

    this.sendEmail(data);

    return "Email send!";
  };

  sendEmail = (data: mailGunData) => {
    const mailGun = createMailgun(apiKey, domain);

    try {
      mailGun.messages().send(data);
    } catch (err) {
      throw new Error("Error sending email.");
    }
  };
}

export const createAuthService = (UserModel: Model<UserRaw>) =>
  new AuthService(UserModel);
