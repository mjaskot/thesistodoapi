import { compose } from "ramda";
import { Router } from "express";

import { UserModel } from "./models/user.model";
import { createAuthService } from "./auth.service";
import { createAuthController } from "./auth.controller";
import { createLoginValidator } from "./validators/login.validator";
import { createPutUserValidator } from "./validators/updateUser.validator";
import { createRegisterValidator } from "./validators/register.validator";
import { createRefreshTokenValidator } from "./validators/refreshToken.validator";

export const createAuthRouter = () => {
  const authRouter = Router();

  const authController = compose(
    createAuthController,
    createAuthService
  )(UserModel);

  authRouter
    .route("/register")
    .post(createRegisterValidator(), authController.register);
  authRouter
    .route("/login")
    .post(createLoginValidator(), authController.signIn);
  authRouter
    .route("/refreshToken")
    .post(createRefreshTokenValidator(), authController.refreshToken);
  authRouter.route("/forgotPassword").post(authController.requestPasswordReset);
  authRouter.route("/resetPassword").post(authController.resetPassword);

  return authRouter;
};

export const createUserRouter = () => {
  const userRouter = Router();

  const authController = compose(
    createAuthController,
    createAuthService
  )(UserModel);

  userRouter
    .route("/:userId")
    .put(createPutUserValidator(), authController.editUser);

  return userRouter;
};
