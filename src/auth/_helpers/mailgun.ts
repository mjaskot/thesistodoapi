import Mailgun = require("mailgun-js");

export const createMailgun = (apiKey: string, domain: string) =>
  Mailgun({ apiKey, domain });
