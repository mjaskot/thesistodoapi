import { createDecipheriv } from "crypto";

import { DecryptedItem } from "./interfaces/encryptedItem.interface";
import { StatusError } from "../../../core/Errors";
import { getEnvVariable, envVariables } from "../../../core/env.initializer";

const hashingAlgorithm = getEnvVariable(envVariables.HASHINGALGORITHM);
const key = getEnvVariable(envVariables.HASHSECRET);
const iv = getEnvVariable(envVariables.CRYPTOIV);

export const decryptor = (encrypted: string): Promise<DecryptedItem> => {
  return new Promise((resolve, reject) => {
    try {
      const decipher = createDecipheriv(hashingAlgorithm, key, iv);
      const decryptedString =
        decipher.update(encrypted, "hex", "utf8") + decipher.final("utf8");
      const decryptedObj: DecryptedItem = JSON.parse(decryptedString);
      resolve(decryptedObj);
    } catch (err) {
      reject(new StatusError(401, "Invalid token"));
    }
  });
};
