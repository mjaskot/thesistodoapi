import { createCipheriv } from "crypto";

//import { DecryptedItem } from "./interfaces/encryptedItem.interface";
import { StatusError } from "../../../core/Errors";
import { getEnvVariable, envVariables } from "../../../core/env.initializer";

const hashingAlgorithm = getEnvVariable(envVariables.HASHINGALGORITHM);
const hashSecret = getEnvVariable(envVariables.HASHSECRET);
const iv = getEnvVariable(envVariables.CRYPTOIV);

export const encryptor = (userId: string): Promise<string> => {
  return new Promise((resolve, reject) => {
    try {
      const payload = JSON.stringify({
        userId
      });

      const cipher = createCipheriv(hashingAlgorithm, hashSecret, iv);
      const encrypted =
        cipher.update(payload, "utf8", "hex") + cipher.final("hex");
      resolve(encrypted);
    } catch (err) {
      reject(new StatusError(401, err.message));
    }
  });
};
