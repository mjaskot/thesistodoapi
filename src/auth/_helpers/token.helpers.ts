import { sign, verify, decode } from "jsonwebtoken";

import { TokenModel } from "../models/token.model";
import { JWTPayload } from "../interfaces/Token.types";
import { StatusError } from "../../core/Errors";
import { getEnvVariable, envVariables } from "../../core/env.initializer";

const secret = getEnvVariable(envVariables.SECRET);

export const generateAccessToken = (userId: string) => {
  const token: JWTPayload = {
    iat: Math.floor(Date.now() / 1000),
    exp: Math.floor(Date.now() / 1000) + 60 * 60 * 24 * 30,
    uid: userId
  };

  return sign(token, secret);
};

export const generateRefreshToken = async (userId: string) => {
  const token: JWTPayload = {
    iat: Math.floor(Date.now() / 1000),
    exp: Math.floor(Date.now() / 1000) + 60 * 60 * 24 * 30,
    uid: userId
  };

  const foundToken = await TokenModel.findOne({ userId });

  if (!foundToken) {
    return await TokenModel.create({
      userId: userId,
      token: await sign(token, secret)
    });
  }

  const { exp } = decode(foundToken.token) as JWTPayload;
  const currentTime = Date.now().valueOf() / 1000;

  if (exp < currentTime) {
    const tokenToUpdate = TokenModel.findOneAndUpdate(
      { userId },
      { token: sign(token, secret) }
    );

    if (!tokenToUpdate) {
      throw new StatusError(404, "RefreshToken not found");
    }
  }

  return foundToken;
};

export const verifyToken = (
  token: string,
  secret: string
): Promise<JWTPayload> => {
  return new Promise((resolve, reject) => {
    verify(token, secret, (err, decoded) => {
      if (err) {
        reject(new StatusError(401, err.message));
      }
      resolve(decoded as JWTPayload);
    });
  });
};

export const refreshToken = async (refreshToken: string): Promise<string> => {
  const tokenExists = await TokenModel.findOne({ token: refreshToken });
  if (!tokenExists) {
    throw new StatusError(404, "Token does not exist in database");
  }
  const { token } = tokenExists;
  const { uid } = await verifyToken(token, secret);

  return await generateAccessToken(uid);
};
