import { UserRaw, RegisteredUser } from "../interfaces/User.types";

export const toRegisteredUser = (
  userRaw: UserRaw,
  token: string
): RegisteredUser => {
  const registeredUser: RegisteredUser = {
    username: userRaw.username,
    email: userRaw.email,
    password: userRaw.password,
    age: userRaw.age,
    adress: userRaw.adress,
    city: userRaw.city,
    country: userRaw.country,
    accessToken: { token }
  };

  return registeredUser;
};
