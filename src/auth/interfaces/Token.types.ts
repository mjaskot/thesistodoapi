export type JWTPayload = {
  [key: string]: string | number;
  iat: number;
  exp: number;
  uid: string;
};
