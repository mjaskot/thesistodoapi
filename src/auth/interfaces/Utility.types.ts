export type LoginResponse = { accessToken: string; refreshToken: string };

export type mailGunData = {
  from: string;
  to: string;
  subject: string;
  text: string;
};
