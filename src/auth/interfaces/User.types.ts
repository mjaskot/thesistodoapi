import { Document } from "mongoose";

export type User = {
  username: string;
  email: string;
  password: string;
  age?: number;
  adress?: string;
  city?: string;
  country?: string;
};

export type UserRaw = Document & User;

export type UserResponse = {
  username: string;
  email: string;
  age?: number;
  adress?: string;
  city?: string;
  country?: string;
};

export type RegisteredUser = {
  username: string;
  email: string;
  password: string;
  age?: number;
  adress?: string;
  city?: string;
  country?: string;
  accessToken: {
    token: string;
  };
};
