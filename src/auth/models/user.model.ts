import { prop, Typegoose } from "typegoose";

class UserSchema extends Typegoose {
  @prop({ required: true })
  username: string;

  @prop({ required: true })
  email: string;

  @prop({ required: true })
  password: string;

  @prop()
  age?: number;

  @prop()
  adress?: string;

  @prop()
  city?: string;

  @prop()
  country?: string;
}

export const UserModel = new UserSchema().getModelForClass(UserSchema, {
  schemaOptions: { collection: "Users", timestamps: true }
});
