import { prop, Typegoose } from "typegoose";

class TokenSchema extends Typegoose {
  @prop({ required: true })
  userId: string;

  @prop({ required: true })
  token: string;
}

export const TokenModel = new TokenSchema().getModelForClass(TokenSchema, {
  schemaOptions: {
    collection: "Tokens",
    timestamps: true
  }
});
